function graph_pcolor(source_name )

    % This function can be used to create an animated heatmap of the
    % desired property, source_name.
       global cells_x ...
            del_x ...
            cells_z ...
            del_z ...
            total_iters ...
            del_t ...
            total_frames  ...
            output_freq_iter  ...
            output_freq_time  ...
            graph_aspect_ratio  ...
            use_every_nth_data_output...
            source_folder ;
   
    source_matrix = importdata(source_name);
    source_matrix = [source_matrix source_matrix(:,size(source_matrix,2))];
    u = importdata(source_folder+"u.dat");
    %u = [u u(:,size(u,2))];
    w = importdata(source_folder+"w.dat");
   % u = [u u(:,size(u,2))];
    
%     horizontal_velocity = importdata("u.dat");

    start_index = 1;
    step_index = cells_z+2;
    
    h = pcolor(flip( source_matrix(start_index :start_index+step_index-1,:) ));
    X = flip(u(start_index :start_index+step_index-1,:));
    Z = flip(w(start_index :start_index+step_index-1,:));
    hold on
    quiv_plot = quiver(X,Z);
    
    %title("2D ocean slice");
    
    %set axis ratios
    %pbaspect([cells_x*del_x cells_z*del_z 1]);    pbaspect([cells_x*del_x cells_z*del_z 1]);
    pbaspect([graph_aspect_ratio 1 1]);

    %no grid
    %h.LineStyle = 'None';
    %interpolate values for a smoother plot
    %h.FaceColor = 'interp';
    
    set_labels_and_ticks(gca);
    
    % https://uk.mathworks.com/help/matlab/ref/colormap.html
    % jet gives a good range of colours, intead of just interpolating two
    colormap( jet); %jet is another good option
    %caxis manual; % set it manually so it stays like this for other iterations
    min_val = min(min(h.CData));
    max_val = max(max(h.CData));
    %caxis([min_val*0.9999,max_val*1.0001]); % fixed the color axis' bounds.


    % start the animation
    for frame_counter = 0:use_every_nth_data_output:total_frames-1
        
        % calculate the mean velocity through the top and bottom pipe
%         surface_hor_vel_average = mean( ...
%             horizontal_velocity(1 + cells_z*frame_counter:11 + cells_z*frame_counter,45:55), ...
%             "all" ...
%         );
%         bottom_hor_vel_average = mean( ...
%             horizontal_velocity(90 + cells_z*frame_counter:100 + cells_z*frame_counter,45:55), ...
%             "all" ...
%         );
       
        info_text_string = "frame num: " + frame_counter + newline + ...
             "time: " + frame_counter * output_freq_iter * del_t + ""; %+ newline +...
%              "top velocity      : " + round(100*surface_hor_vel_average)/100) + newline + ...
%             "bottom velocity: " + round(100*bottom_hor_vel_average)/100 ...
            %);
        dim = [.2 .5 .3 .3];
       info_text = annotation('textbox',dim,'String',info_text_string,'FitBoxToText','on');
          
       uistack(info_text,'top'); %put the info text on top of the plot
        
        colorbar
        new_data = flip(source_matrix(start_index :start_index+step_index -1,:));
        h.CData =   new_data;
        
        X = flip(u(start_index :start_index+step_index-1,:));

        Z = flip(w(start_index :start_index+step_index-1,:)).*1000000;
        quiv_plot.WData = Z;

        quiv_plot.UData = X;

        drawnow
        pause(0.01) % can be used to slow down the animation
        
%         if frame_counter == 12
%             print -depsc pre_shooting_upwards
%         end
%         if frame_counter == 22
%             print -depsc shooted_upwards
%         end
       delete(info_text) % so that the numbers don't get written on top of each other 
       
       start_index = start_index + step_index;

    end
    
end