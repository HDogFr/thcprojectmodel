%{
This reads the parameters from the Fortran code and calls 
the graphing functions used to analyse the data.
%}

%source_folder = "..\AdvancedOMExercises\Updated_Exercises_March_2015\Exercise 5\";
%source_folder = "./symmetric_forcing_updated_2/";
source_folder = "";

% the model parameters of interest are stored in parameters.txt
params_fileID   = fopen(source_folder + 'parameters.txt','r');
params          = fscanf(params_fileID, "%f");
fclose(params_fileID);

% defining global variables across the Matlab project
global cells_x del_x cells_z del_z total_iters del_t total_frames output_freq_iter output_freq_time add_asymmetry source_folder;
cells_x         = params(1); % how many horizontal cells
del_x           = params(2); % horizontal cell width
cells_z         = params(3);
del_z           = params(4);
total_iters     = params(5); % how many iterations the simulation ran for
del_t           = params(6); % time step
total_frames    = params(7); % how many times we wrote the data to disk
output_freq_iter= params(8); % how many time steps between data outputs
%add_asymmetry   = params(9); %how many iterations until the asymmetry was added
output_freq_time= output_freq_iter*del_t; %how long (in seconds) between data outputs

% global visual parameters
global graph_aspect_ratio use_every_nth_data_output;
graph_aspect_ratio = 2.7; %width to height ratio
use_every_nth_data_output = 1; %higher numbers speed up the animations

% data analysis functions

%graph_heatmap("temp.dat");

graph_quiver(source_folder);

%one_cell_over_time("u.dat",42,2);

%graph_pcolor(source_folder +"sal.dat")