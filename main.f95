!g95 -o run .\main.f95 .\parameters.f95 .\subroutines.f95
!./run.exe

PROGRAM THC2d

  USE parameters
  USE subroutines

  ! useful for debugging
  IMPLICIT NONE

  ! local parameters and variables
  INTEGER :: total_iters, output_freq !output data at every output_freq th iteration
  REAL, PARAMETER :: total_sim_time = 500*365*24*3600.0 ! seconds
  REAL :: current_sim_time
  INTEGER :: i,k
  REAL :: start_time, finish_time


  ! ------------ initialisation -------------------------
  CALL CPU_TIME(start_time)
  
  ! initialise the model ready for the first time step
  CALL initialise

  ! runtime parameters
  total_iters = INT(total_sim_time/del_t)
  current_sim_time = 0.0
  output_freq = 5000; !INT(24*3600/del_t);
  verbose = .TRUE.

  ! open files for output
  OPEN (10, file='q.dat', form='formatted')
  OPEN (20, file='u.dat', form='formatted')
  OPEN (30, file='w.dat', form='formatted')
  OPEN (40, file='sal.dat', form='formatted')
  OPEN (50, file='temp.dat', form='formatted')
  OPEN (60, file='wet.dat', form='formatted')
  OPEN (70, file='rho.dat', form='formatted')



  ! DO i = 3,8
  ! DO k = 5, 15
  !       temp(i,k) = TEMP_0*0.9
  ! END DO
  ! END DO

  ! DO i = cells_z -10, cells_z
  !   DO k = 0, cells_x + 1, 4
  !     temp(i,k) = TEMP_0 
  !   END DO
  !   END DO



  ! ------------ introduce noise into fields  ------------
  ! this is to aid in symmetry breaking
  ! DO i = 0, cells_z + 1
  ! DO k = 0, cells_x + 1
  !   ! temp(i,k) =temp(i,k)*( 1 + rand()*0.01)
  !   ! sal(i,k) = sal(i,k)*( 1 + rand()*0.01)
  !   u(i,k) = rand()*0.0001
  !   w(i,k) = rand()*0.00001
  ! END DO
  ! END DO
 

  ! writing the parameters to a file for the matlab script to use
  OPEN (9, file="parameters.txt",action="write",status="replace")
  WRITE (9,*) cells_x
  WRITE (9,*) del_x
  WRITE (9,*) cells_z
  WRITE (9,*) del_z
  WRITE (9,*) total_iters
  WRITE (9,*) del_t
  WRITE (9,*) INT(total_iters/output_freq) !number of times we output the arrays
  WRITE (9,*) output_freq
  WRITE (9,*) add_asymmetry
  CLOSE (9)

  ! output the logical array wetness (dry land vs ocean):
  DO i = 0, cells_z + 1 
    WRITE (60, '(103L1)') (wet(i, k), k=0, cells_x+1)
  END DO


  CLOSE (60)


  ! --------------------- simulation loop --------------------------

  DO current_iter = 1, total_iters

    current_sim_time = current_sim_time + del_t

  ! prognostic equations
    CALL update

    ! try data output
    IF (MOD(current_iter, output_freq) == 0) THEN
      ! Write to files
      ! At each data output, there will be cells_z new rows, each one cells_x wide
      DO i = 0, cells_z+1
        WRITE (10, '(101F12.6)') (q(i, k), k=0, cells_x+1)
        WRITE (20, '(101F12.6)') (u(i, k), k=0, cells_x+1)
        WRITE (30, '(101F12.6)') (w(i, k), k=0, cells_x+1)
        WRITE (40, '(101F12.6)') (sal(i, k), k=0, cells_x+1)
        WRITE (50, '(101F12.6)') (temp(i, k), k=0, cells_x+1)
        WRITE (70, '(101F12.6)') (rho(i, k), k=0, cells_x+1)
      END DO

      !to the terminal
      WRITE (6, *) "Data output at time = ", current_sim_time
      WRITE (6, *) "percentage complete", current_sim_time/total_sim_time * 100

      ! IF (current_iter > add_asymmetry) THEN
      !   WRITE (6,*) "ASYMMETRIC FORCING HAS BEGUN"
      ! END IF

    END IF

  END DO ! end of iteration loop


  CALL CPU_TIME(finish_time)
  WRITE (6,*)   "Time elapsed (seconds): " , finish_time-start_time


END PROGRAM THC2d
