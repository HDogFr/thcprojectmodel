MODULE subroutines

  USE parameters

  IMPLICIT NONE

  CONTAINS

    FUNCTION rho_from_s_t(s,t) 
      ! Calculate density from salinity and temperature values
      REAL :: rho_from_s_t
      REAL :: s
      REAL :: t
      rho_from_s_t = RHO_0 * (1 - THRML_XPSN * (t - TEMP_0) + SAL_CONTR*(s-SAL_0))
      RETURN
    END FUNCTION

    SUBROUTINE initialise

      INTEGER :: n_bot, i,k

      INTEGER :: reason


      ! set intial values in the arrays
      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1

        wet(i,k) = .true.

        sal_new(i,k) = 0.0
        temp_new(i,k) = 0.0
        ! rhon(i,k) = 0.0
        p(i,k) = 0.0
        q(i,k) = 0.0
        del_q(i,k) = 0.0

        u_star(i,k) = 0.0
        u_new(i,k) = 0.0
        w_star(i,k) = 0.0
        w_new(i,k) = 0.0

      END DO
      END DO

      IF (startup_from_previous_data) THEN 
          
        OPEN (10, file=startup_data_path//'q.dat', form='formatted')
        OPEN (20, file=startup_data_path//'u.dat', form='formatted')
        OPEN (30, file=startup_data_path//'w.dat', form='formatted')
        OPEN (40, file=startup_data_path//'sal.dat', form='formatted')
        OPEN (50, file=startup_data_path//'temp.dat', form='formatted')
        OPEN (60, file=startup_data_path//'wet.dat', form='formatted')
        OPEN (70, file=startup_data_path//'rho.dat', form='formatted')
        
        !read the "wet" logical array
        ! DO i = 0, cells_z + 1 
        !   READ (60, '(103L1)') wet(i, 0:cells_x+1)
        ! END DO

        DO
          DO i = 0, cells_z + 1
            READ(10, '(101F12.6)', IOSTAT=reason) q(i, 0:cells_x + 1)
            READ(20, '(101F12.6)', IOSTAT=reason) u(i, 0:cells_x + 1)
            READ(30, '(101F12.6)', IOSTAT=reason) w(i, 0:cells_x + 1)
            READ(40, '(101F12.6)', IOSTAT=reason) sal(i, 0:cells_x + 1)
            READ(50, '(101F12.6)', IOSTAT=reason) temp(i, 0:cells_x + 1)
            READ(70, '(101F12.6)', IOSTAT=reason) rho(i, 0:cells_x + 1)
          END DO

          IF (.not. reason ==  0) THEN
              EXIT
          END if
        END DO

        CLOSE(10)
        CLOSE(20)
        CLOSE(30)
        CLOSE(40)
        CLOSE(50)
        CLOSE(60)
        CLOSE(70)


      ELSE !if we aren't starting using the previous runs data then

        DO i = 0, cells_z + 1
          DO k = 0, cells_x + 1
            u(i,k) = 0.0
            w(i,k) = 0.0
            sal(i,k) = SAL_0
            temp(i,k) = TEMP_0
            rho(i,k) = rho_from_s_t(SAL_0,TEMP_0)
            wet(i,k) = .TRUE.
          END do
        END DO
      END IF

      DO k = 0,cells_x+1
        u_sum(k) = 0.0
      END DO

      ! lateral boundaries are closed
      DO i = 0, cells_z + 1
        wet(i, 0) = .FALSE.
        wet(i, cells_x + 1 ) = .FALSE.
      END DO

      DO k = 0, cells_x + 1
        wet(cells_z+1, k) = .FALSE. ! solid bottom
        !wet(0, k) = .FALSE.
      END DO


     
      ! coefficients for SOR
      omega_SOR = 1.4
      pressure_epsilon = 1.e-3

      DO i = 1, cells_z
      DO k = 1, cells_x
         at(i, k) = del_x/del_z
         ab(i, k) = del_x/del_z
         ae(i, k) = del_z/del_x
         aw(i, k) = del_z/del_x
         IF (.not. wet(i, k - 1)) aw(i, k) = 0.0
         IF (.not. wet(i, k + 1)) ae(i, k) = 0.0
         IF (.not. wet(i + 1, k)) ab(i, k) = 0.0
         IF (.not. wet(i - 1, k)) at(i, k) = 0.0
         a_total(i, k) = ab(i, k) + at(i, k) + ae(i, k) + aw(i, k)
      END DO
      END DO


    END SUBROUTINE initialise



    SUBROUTINE update
      !This is the primary subroutine used to update to the next time step.

      ! local parameters
      REAL :: pressure_error, old_delta_q, new_delta_q, term1
      REAL :: pressure_x, pressure_z
      REAL :: advection_x(0:cells_z + 1, 0:cells_x + 1), advection_z(0:cells_z + 1, 0:cells_x + 1)
      INTEGER :: max_iters, SOR_stop_index, index_SOR
      REAL :: continuity_divergence, dif1, dif2, difh, difz, diffusion
      REAL :: div, div1, div2
      INTEGER :: i,k

      REAL :: initial_pressure_error = 0.0
      REAL :: new_sal, new_temp
      REAL :: change_in_fields


       ! ----------------- FORCING --------------------- !
     
      ! DO k = 0, 10 !cells_x 
      !   new_temp = (1. +   0.1*sin(PI* (k)/(20)))*TEMP_0
      !   new_sal = (1.1-0.01 + 0.01*sin(PI* k/20))*SAL_0
      !   temp(1,k) =  new_temp
      !   sal(1,k)  =  new_sal
 
      !   temp(1,cells_x+1-k) =  new_temp
      !   sal(1,cells_x+1-k)  = new_sal
      ! END DO

      DO k = 0,cells_x+1
        new_temp = TEMP_0*(1 + 0.1*( sin(PI*k /(cells_x+1))-1))
        new_sal  =  SAL_0*(1 + 0.01*( sin(PI*k /(cells_x+1))-1)) 

        IF ( k > INT(cells_x*0.75)) THEN !current_iter > add_asymmetry .and.
          new_sal = new_sal*ASYMMETRY_MULTIPLIER
        END IF


        temp(1,k) = new_temp
        sal(1,k) = new_sal
      END DO

      ! DO k = 0, cells_x +1
      !   change_in_fields = cos(k*1.0/(cells_x+1) * PI)*0.2

      !   sal(1,k)  = sal(1,k)  + ( SAL_0*(1+change_in_fields)  - sal(1,k))*del_t*0.01*SALINITY_CHANGE_COEFFICIENT
      !   temp(1,k) = temp(1,k) + ( TEMP_0*(1+change_in_fields) - temp(1,k))*del_t*0.01*1
      ! END DO

      ! IF (current_iter > 50000) THEN
      !   DO k = INT(cells_x*0.75), cells_x
      !     sal(1,k) = SAL_0*1.02 !sal(1,k)*1.002
      !   END do
      !   write(6,*) "Current iter is: ", current_iter
      ! END IF





     
      


      ! -------------- salinity update --------------
      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1
          c_u_pos(i, k) = 0.5*(u(i, k) + abs(u(i, k)))*del_t/del_x
          c_u_neg(i, k) = 0.5*(u(i, k) - abs(u(i, k)))*del_t/del_x
          c_w_pos(i, k) = 0.5*(w(i, k) + abs(w(i, k)))*del_t/del_z
          c_w_neg(i, k) = 0.5*(w(i, k) - abs(w(i, k)))*del_t/del_z

          !fill the generic field with values of salinity
          F(i, k) = sal(i, k)

      END DO
      END DO

      CALL advect ! salinity advection

      DO i = 1, cells_z
      DO k = 1, cells_x
          continuity_divergence = del_t*F(i, k)*( &
            (u(i, k) - u(i, k - 1))/del_x + &
            (w(i, k) - w(i + 1, k))/del_z &
          )

          ! add diffusion terms
          dif1 = 0.0
          IF (wet(i, k + 1)) dif1 = (sal(i, k + 1) - sal(i, k))/del_x
          dif2 = 0.0
          IF (wet(i, k - 1)) dif2 = (sal(i, k) - sal(i, k - 1))/del_x
          difh = KH*(dif2 - dif1)/del_x
          dif1 = 0.0
          IF (wet(i - 1, k)) dif1 = (sal(i - 1, k) - sal(i, k))/del_z
          dif2 = 0.0
          IF (wet(i + 1, k)) dif2 = (sal(i, k) - sal(i + 1, k))/del_z
          difz = KZ*(dif2 - dif1)/del_z
          diffusion = -del_t*(difh + difz)
          ! combining advection, diffusion, continuity equation
          sal_new(i, k) = sal(i, k) + F_new(i, k) + continuity_divergence + diffusion
      END DO
      END DO

      ! boundary conditions
      DO i = 0, cells_z + 1
        sal_new(i, 0) = sal_new(i, 1) !left
        sal_new(i, cells_x + 1) = sal_new(i, cells_x) !right
      END DO

      DO k = 0, cells_x + 1
        sal_new(0, k) = sal_new(1, k) !top
        sal_new(cells_z + 1, k) = sal_new(cells_z, k) !bottom
      END DO

      DO k = 0, cells_x + 1
      DO i = 0, cells_z + 1
        sal(i, k) = sal_new(i, k)
      END DO
      END DO

      

      ! ----------- temperature update ------------
      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1
        !fill the generic field with values of temperature
        F(i, k) = temp(i, k)
      END DO
      END DO

      CALL advect ! temperature advection

      DO i = 1, cells_z
      DO k = 1, cells_x
          continuity_divergence = del_t*F(i, k)*( &
              (u(i, k) - u(i, k - 1))/del_x + &
              (w(i, k) - w(i + 1, k))/del_z &
          )

          ! add diffusion terms
          dif1 = 0.0
          IF (wet(i, k + 1)) dif1 = (temp(i, k + 1) - temp(i, k))/del_x
          dif2 = 0.0
          IF (wet(i, k - 1)) dif2 = (temp(i, k) - temp(i, k - 1))/del_x
          difh = KH*(dif2 - dif1)/del_x
          dif1 = 0.0
          IF (wet(i - 1, k)) dif1 = (temp(i - 1, k) - temp(i, k))/del_z
          dif2 = 0.0
          IF (wet(i + 1, k)) dif2 = (temp(i, k) - temp(i + 1, k))/del_z
          difz = KZ*(dif2 - dif1)/del_z
          diffusion = -del_t*(difh + difz) !THERE WAS A MISSING NEG SIGN HERE

          ! IF (i == cells_z - 11 .and. k == 45) THEN
          !   WRITE (6,*) "At cells_z - 11, k == 45"
          !   WRITE (6,*) dif1
          !   WRITE (6,*) dif2
          !   WRITE (6,*) difz
         
          !   WRITE (6,*) difh
          !   WRITE (6,*) diffusion
          ! END IF

          ! IF (i == cells_z - 11 .and. k == 40) THEN
          !   WRITE (6,*) "At cells_z - 11, k == 40"
          !   WRITE (6,*) dif1
          !   WRITE (6,*) dif2
          !   WRITE (6,*) difz
         
          !   WRITE (6,*) difh
          !   WRITE (6,*) diffusion
          ! END IF

          ! combining advection, diffusion, continuity equation
          temp_new(i, k) = temp(i, k) + F_new(i, k) + continuity_divergence + diffusion
      END DO
      END DO

      ! boundary conditions
      DO i = 0, cells_z + 1
        temp_new(i, 0) = temp_new(i, 1)
        temp_new(i, cells_x + 1) = temp_new(i, cells_x)
      END DO

      DO k = 0, cells_x + 1
        temp_new(0, k) = temp_new(1, k)
        temp_new(cells_z + 1, k) = temp_new(cells_z, k)
      END DO

      ! updating temperature
      DO k = 0, cells_x + 1
      DO i = 0, cells_z + 1
        temp(i, k) = temp_new(i, k)
      END DO
      END DO

    

      !---------------- calculate hydrostatic pressure ---------------

      
      DO k = 0, cells_x + 1
      DO i = 0, cells_z + 1
        rho(i, k) = rho_from_s_t(sal(i, k),temp(i,k))
      END DO
      END DO

      DO i = 0, cells_z + 1
        rho(i, 0) = rho(i, 1)
        rho(i, cells_x + 1) = rho(i, cells_x)
      END DO

      DO k = 0, cells_x + 1
        rho(0, k) = rho(1, k)
        rho(cells_z + 1, k) = rho(cells_z, k)
      END DO


      DO k = 0, cells_x + 1
        p(0, k) = 0.0
        DO i = 1, cells_z + 1
          p(i, k) =  p(i - 1, k) + 0.5*(rho(i - 1, k) + rho(i, k))*G*del_z
        END DO
      END DO

      ! ---------------- calculate the nonlinear terms for u-momentum equation 
      DO i = 0, cells_z + 1
      DO k = 0, cells_x
        c_u_pos(i, k) = 0.25*(u(i, k) + u(i, k + 1) + abs(u(i, k)) + abs(u(i, k + 1)))*del_t/del_x
        c_u_neg(i, k) = 0.25*(u(i, k) + u(i, k + 1) - abs(u(i, k)) - abs(u(i, k + 1)))*del_t/del_x
        c_w_pos(i, k) = 0.25*(w(i, k) + w(i, k + 1) + abs(w(i, k)) + abs(w(i, k + 1)))*del_t/del_z
        c_w_neg(i, k) = 0.25*(w(i, k) + w(i, k + 1) - abs(w(i, k)) - abs(w(i, k + 1)))*del_t/del_z
      END DO
      END DO

      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1
        F(i, k) = u(i, k)
      END DO
      END DO

    ! advection of u
      CALL advect

      DO i = 1, cells_z
      DO k = 1, cells_x
        div1 = 0.5*(u(i, k + 1) - u(i, k - 1))/del_x
        div2 = 0.5*(w(i, k) + w(i, k + 1) - w(i + 1, k) - w(i + 1, k + 1))/del_z
        div = del_t*F(i, k)*(div1 + div2)
        advection_x(i, k) = F_new(i, k) + div
      END DO
      END DO

      ! ---------------- calculate the nonlinear terms for w-momentum equation ----------------
      DO i = 0, cells_z
      DO k = 0, cells_x + 1
        c_u_pos(i, k) = 0.25*(u(i, k) + u(i - 1, k) + abs(u(i, k)) + abs(u(i - 1, k)))*del_t/del_x
        c_u_neg(i, k) = 0.25*(u(i, k) + u(i - 1, k) - abs(u(i, k)) - abs(u(i - 1, k)))*del_t/del_x
        c_w_pos(i, k) = 0.25*(w(i, k) + w(i - 1, k) + abs(w(i, k)) + abs(w(i - 1, k)))*del_t/del_z
        c_w_neg(i, k) = 0.25*(w(i, k) + w(i - 1, k) - abs(w(i, k)) - abs(w(i - 1, k)))*del_t/del_z
      END DO
      END DO

      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1
        F(i, k) = w(i, k)
      END DO
      END DO

      ! advection of w 
      CALL advect
      
      DO i = 1, cells_z
      DO k = 1, cells_x
          div1 = 0.5*(u(i, k) + u(i - 1, k) - u(i, k - 1) - u(i - 1, k - 1))/del_x
          div2 = 0.5*(w(i - 1, k) - w(i + 1, k))/del_z
          div = del_t*F(i, k)*(div1 + div2)
          advection_z(i, k) = F_new(i, k) + div
      END DO
      END DO

      ! DO i = 0, cells_z + 1
      !   u(i,0) = 0.0 !no slip
      !   u(i,cells_x+1) = 0.0
      !   w(i,cells_x+1) = 0.0
      ! END DO

      ! DO k = 0, cells_x + 1
      !   u(0,k) = u(1,k) !free slip
      !   w(0,k) = 0.0 !rigid lid 
      !   w(1,k) = 0.0 !rigid lid 
      !   u(cells_z+1,k) = u(cells_z,k)
      !   w(cells_z+1,k) = w(cells_z+1,k)
      ! END DO

      !rigid lid approximation
      ! DO k = 0, cells_x + 1
      !   w(0, k) = 0.0
      ! END DO

      !---------------- calculate u_star and w_star----------------
      DO i = 1, cells_z
      DO k = 1, cells_x
          IF (wet(i, k)) THEN
            pressure_x = -RHO_DEL_X_INV*(q(i, k + 1) - q(i, k)) - RHO_DEL_X_INV*(p(i, k + 1) - p(i, k))
            IF (wet(i, k + 1)) u_star(i, k) = u(i, k) + del_t*pressure_x + advection_x(i, k)
            pressure_z = -RHO_DEL_Z_INV*(q(i - 1, k) - q(i, k))
            IF (wet(i - 1, k)) w_star(i, k) = w(i, k) + del_t*pressure_z + advection_z(i, k)
          END IF
      END DO
      END DO


      ! DO i = 0, cells_z + 1
      !   u_star(i,0) = 0.0 !no slip
      !   w_star(i,0) = 0.0
      !   u_star(i,cells_x+1) = 0.0
      !   w_star(i,cells_x+1) = 0.0
      ! END DO

      ! DO k = 0, cells_x + 1
      !   u_star(0,k) = u_star(1,k) !free slip
      !   w_star(0,k) = 0.0 !rigid lid 
      !   w_star(1,k) = 0.0 !rigid lid 
      !   u_star(cells_z+1,k) = u(cells_z,k)
      !   w_star(cells_z+1,k) = w_star(cells_z+1,k)
      ! END DO

      ! ---------------- calculate right-hand side of Poisson equation----------------
      DO i = 1, cells_z
      DO k = 1, cells_x
          q_star(i, k) = -1.*rho_from_s_t(SAL_0,TEMP_0)/del_t*(  &
          &  (u_star(i, k) - u_star(i, k - 1))*del_z + &
          &  (w_star(i, k) - w_star(i + 1, k))*del_x)
      END DO
      END DO


      ! ---------------- starting SOR iteration --------------------

      max_iters = 8000


      DO index_SOR = 1, max_iters
        pressure_error = 0.0

        ! Predict pressure correction
        
        DO i = 1, cells_z
        DO k = 1, cells_x
          IF (wet(i, k)) THEN

              old_delta_q = del_q(i, k) ! old delta q
              term1 = q_star(i, k) + &
              &      at(i, k)*del_q(i - 1, k) + ab(i, k)*del_q(i + 1, k) + &
              &      aw(i, k)*del_q(i, k - 1) + ae(i, k)*del_q(i, k + 1)
              new_delta_q = (1.0 - omega_SOR)*old_delta_q + omega_SOR*term1/a_total(i, k) ! new delta q
              del_q(i, k) = new_delta_q

              ! if (index_SOR == 10 .and. i == 1) THEN
              !   write (6,*) "10th iteration", new_delta_q , old_delta_q, ABS(new_delta_q - old_delta_q), i,k
              ! END IF

              ! if (index_SOR == 500 .and. i == 1) THEN
              !   write (6,*) "500th iteration", new_delta_q , old_delta_q, ABS(new_delta_q - old_delta_q), i,k
              ! END IF

              pressure_error = MAX(ABS(new_delta_q - old_delta_q), pressure_error)

          END IF

        END DO
        END DO

        ! Predict new velocities
        DO i = 1, cells_z
        DO k = 1, cells_x
            IF (wet(i, k)) THEN
              pressure_x = -RHO_DEL_X_INV*(del_q(i, k + 1) - del_q(i, k))
              IF (wet(i, k + 1)) u_new(i, k) = u_star(i, k) + del_t*pressure_x
              pressure_z = -RHO_DEL_Z_INV*(del_q(i - 1, k) - del_q(i, k))
              IF (wet(i - 1, k)) w_new(i, k) = w_star(i, k) + del_t*pressure_z
            END IF
        END DO
        END DO

        ! STEP 3a: predict depth-integrated flow
        DO k = 1,cells_x
          u_sum(k) = 0.
          DO i = 1,cells_z
            u_sum(k) = u_sum(k) + del_Z*u_new(i,k)
          END DO
        END DO

        ! lateral boundary conditions
        u_sum(cells_x) = 0.0

        DO k = 1, cells_x
          del_q(0, k) = -del_T*rho_from_s_t(SAL_0,TEMP_0)*G*(u_sum(k)-u_sum(k-1))/del_x
        END DO

        IF (index_SOR == 1) THEN
          initial_pressure_error = pressure_error
        END IF
        
 
        ! Has convergence occured
        IF (pressure_error <= pressure_epsilon) THEN
          SOR_stop_index = index_SOR
          GOTO 33
        END IF

      END DO

      IF (verbose) THEN
        WRITE (*,*) "EXCEEDED INTERACTION LIMIT"
      END IF

      GOTO 34 ! skips over the following line
       ! comment the goto to avoid verbosity

      33 IF (verbose) THEN
      
          WRITE (*, *) "No. of Interactions =>", SOR_stop_index
          !WRITE (6,*) "Initial pressure error: ", initial_pressure_error
          
        END IF
      34    CONTINUE

      
      ! Update for the next time step
      DO i = 1, cells_z
      DO k = 1, cells_x
        q(i, k) = q(i, k) + del_q(i, k)

        u(i, k) = u_new(i, k)
        w(i, k) = w_new(i, k)
      END DO
      END DO

      DO k = 0, cells_x + 1
        q(0, k) = q(0, k) + del_q(0, k)

        w(cells_z+1,k) = w(cells_z,k)
        u(cells_z+1,k) = u(cells_z,k)
      END DO

 
      ! lateral boundary conditions
      DO i = 0, cells_z + 1 
        u(i,cells_x+1) = 0.0
        u(i,0) = 0.0
        w(i,cells_x+1) = 0.0
        w(i,0) = 0.0
        
        q(i, cells_x + 1) = q(i, cells_x)
        q(i, 0) = q(i, 1)
      END DO


     

    END SUBROUTINE update

    SUBROUTINE advect
      ! Computes the advection for a field F.

      ! The ratio of the sucessive upwind gradients
      REAL :: r_x_pos(0:cells_z + 1, 0:cells_x + 1), r_x_neg(0:cells_z + 1, 0:cells_x + 1)
      REAL :: r_z_pos(0:cells_z + 1, 0:cells_x + 1), r_z_neg(0:cells_z + 1, 0:cells_x + 1)
      
      ! These are temporary variables to make the subroutine easier to read
      REAL :: del_F, term1, term2, term3, term4

      ! west, east ,bottom, top, and pos or neg for each of the cell walls
      REAL :: F_w_pos, F_w_neg, F_e_pos, F_e_neg, F_b_pos, F_b_neg, F_t_pos, F_t_neg 

      INTEGER :: i,k


      ! initialising the r arrays
      DO i = 0, cells_z + 1
      DO k = 0, cells_x + 1
        r_x_pos(i, k) = 0.0
        r_x_neg(i, k) = 0.0
        r_z_pos(i, k) = 0.0
        r_z_neg(i, k) = 0.0
      END DO
      END DO

      ! Filling in the values of r
      ! The positive direction ones
      DO i = 1, cells_z
      DO k = 1, cells_x
          del_F = F(i, k + 1) - F(i, k) 
          IF (ABS(del_F) > 0.0) r_x_pos(i, k) = (F(i, k) - F(i, k - 1))/del_F
          del_F = F(i - 1, k) - F(i, k)
          IF (ABS(del_F) > 0.0) r_z_pos(i, k) = (F(i, k) - F(i + 1, k))/del_F
      END DO
      END DO

      ! The negative direction for x
      DO i = 1, cells_z
      DO k = 0, cells_x - 1
          del_F = F(i, k + 1) - F(i, k)
          IF (ABS(del_F) > 0.0) r_x_neg(i, k) = (F(i, k + 2) - F(i, k + 1))/del_F
      END DO
      END DO

      ! The negative direction for z
      DO i = 2, cells_z + 1
      DO k = 1, cells_x
          del_F = F(i - 1, k) - F(i, k)
          IF (ABS(del_F) > 0.0) r_z_neg(i, k) = (F(i - 2, k) - F(i - 1, k))/del_F
      END DO
      END DO

      DO i = 1, cells_z
      DO k = 1, cells_x
        F_w_pos = F(i,k-1) + 0.5*PSI(r_x_pos(i,k-1))*(1.0 - c_u_pos(i,k-1))*(F(i,k  ) - F(i,k-1))
        F_w_neg = F(i,k)   - 0.5*PSI(r_x_neg(i,k-1))*(1.0 + c_u_neg(i,k-1))*(F(i,k  ) - F(i,k-1))

        F_e_pos = F(i,k)   + 0.5*PSI(r_x_pos(i,k  ))*(1.0 - c_u_pos(i, k ))*(F(i,k+1) - F(i,k))
        F_e_neg = F(i,k+1) - 0.5*PSI(r_x_neg(i,k  ))*(1.0 + c_u_neg(i, k ))*(F(i,k+1) - F(i,k))

        F_b_pos = F(i+1,k) + 0.5*PSI(r_z_pos(i+1,k))*(1.0 - c_w_pos(i+1,k))*(F(i,k  ) - F(i+1,k))
        F_b_neg = F(i,k)   - 0.5*PSI(r_z_neg(i+1,k))*(1.0 + c_w_neg(i+1,k))*(F(i,k  ) - F(i+1,k))

        F_t_pos = F(i, k)  + 0.5*PSI(r_z_pos(i, k ))*(1.0 - c_w_pos(i, k ))*(F(i-1, k) - F(i,k))
        F_t_neg = F(i-1,k) - 0.5*PSI(r_z_neg(i, k ))*(1.0 + c_w_neg(i, k ))*(F(i-1, k) - F(i,k))

        term1 = c_u_pos(i,k-1)*F_w_pos + c_u_neg(i,k-1)*F_w_neg
        term2 = c_u_pos(i,k  )*F_e_pos + c_u_neg(i,k  )*F_e_neg
        term3 = c_w_pos(i+1,k)*F_b_pos + c_w_neg(i+1,k)*F_b_neg
        term4 = c_w_pos(i,k  )*F_t_pos + c_w_neg(i,k  )*F_t_neg

        F_new(i, k) = term1 - term2 + term3 - term4
      END DO
      END DO

    END SUBROUTINE advect


    REAL FUNCTION psi(r)
      !Superbee flux limiter function
      REAL, INTENT(IN) :: r
      !psi = max(min(2r,1), min(r,2), 0)
      psi = MAX( MIN(2.0*r, 1.0), MIN(r, 2.0), 0.0)
      RETURN
    END FUNCTION psi

END MODULE subroutines