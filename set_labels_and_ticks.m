function set_labels_and_ticks(current_graph)
    % Used to consistently add the desired x and y labels and ticks for any
    % graph; heatmap, quiver etc ...

    global cells_x ...
            del_x ...
            cells_z ...
            del_z;
    
    x_tick_steps = 5;
    y_tick_steps = 3;

    current_graph.XTick = (0:x_tick_steps:(cells_x));
    current_graph.XTickLabel = (del_x*(0:x_tick_steps:cells_x)./1000);
    current_graph.XLabel.String = ("Horizontal position (kilometres)");
    
    current_graph.YTick = (0:y_tick_steps:(cells_z+2));
    current_graph.YTickLabel = (flip(-del_z*(0:y_tick_steps:(cells_z+2))));
    current_graph.YLabel.String = ("Vertical depth (metres)");
end

