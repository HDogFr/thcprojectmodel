MODULE parameters
  ! steps in time, and space
  REAL, PARAMETER :: del_t = 300.0! seconds
  REAL, PARAMETER :: del_x = 375000.0 ! meters
  REAL, PARAMETER :: del_z = 250.0 ! meters 
  INTEGER, PARAMETER :: cells_x = 33 ! cells in x axis
  INTEGER, PARAMETER :: cells_z = 17 ! cells in z axis

  INTEGER :: current_iter !the current iteration that we are on
  INTEGER :: add_asymmetry = 0 !after this many iterations, add the asymmetry
  REAL :: ASYMMETRY_MULTIPLIER = 1.001

  LOGICAL :: startup_from_previous_data = .true. 
  CHARACTER(len = *), PARAMETER :: startup_data_path = "./symmetric_forcing_updated_2/"


  ! Physical constants
  REAL, PARAMETER :: G = 9.81 ! gravity
  REAL, PARAMETER :: RHO_0 = 1028.0 ! reference density
  REAL, PARAMETER :: THRML_XPSN = 0.00021
  REAL, PARAMETER :: TEMP_0 = 20.0 ! reference temperature
  REAL, PARAMETER :: SAL_CONTR = 0.00075
  REAL, PARAMETER :: SAL_0 = 35.0 !See note on the grid value of salinity
  REAL, PARAMETER :: KH = 1.e-4   ! horizontal eddy diffusivity
  REAL, PARAMETER :: KZ = 1.e-4   ! vertical eddy diffusivity
  REAL, PARAMETER :: RHO_DEL_X_INV = 1.0/(RHO_0*del_x)
  REAL, PARAMETER :: RHO_DEL_Z_INV = 1.0/(RHO_0*del_z)
  REAL, PARAMETER :: PI = 3.14159265 ! pi
  REAL, PARAMETER :: SIDE_FRICTION_COEFFICIENT = 0.7 !TODO
  REAL, PARAMETER :: SALINITY_CHANGE_COEFFICIENT = 0.0001 




  ! grid values
  REAL :: u(0:cells_z + 1, 0:cells_x + 1) ! x speed
  REAL :: w(0:cells_z + 1, 0:cells_x + 1) ! z speed
  REAL :: sal(0:cells_z + 1, 0:cells_x + 1) ! salinity. 
  REAL :: sal_new(0:cells_z + 1, 0:cells_x + 1) ! salinity. 
    !The average salinity of seawater is S = 35 which means that SW is 3.5% salt and 96.5% H2O by weight. 
  REAL :: temp(0:cells_z + 1, 0:cells_x + 1) ! temperature
  REAL :: temp_new(0:cells_z + 1, 0:cells_x + 1) ! temperature
  REAL :: rho(0:cells_z + 1, 0:cells_x + 1) ! density
  REAL :: rhon(0:cells_z + 1, 0:cells_x + 1) ! density
  REAL :: p(0:cells_z + 1, 0:cells_x + 1) ! hydrostatic pressure
  REAL :: q(0:cells_z + 1, 0:cells_x + 1) ! nonhydrostatic pressure
  LOGICAL :: wet(0:cells_z + 1, 0:cells_x + 1) ! whether there is water in cell
  REAL :: depth(cells_x) !depth at each x pos

  ! parameters for S.O.R iteration
  REAL :: u_star(0:cells_z + 1, 0:cells_x + 1)
  REAL :: u_new(0:cells_z + 1, 0:cells_x + 1)
  REAL :: u_sum(0:cells_x+1) ! depth-integrated flow
  REAL :: w_star(0:cells_z + 1, 0:cells_x + 1)
  REAL :: w_new(0:cells_z + 1, 0:cells_x + 1)
  REAL :: q_star(cells_z, cells_x)
  REAL :: del_q(0:cells_z + 1, 0:cells_x + 1)
  REAL :: omega_SOR
  REAL :: at(cells_z, cells_x), ab(cells_z, cells_x)
  REAL :: ae(cells_z, cells_x), aw(cells_z, cells_x)
  REAL :: a_total(cells_z, cells_x)
  REAL :: pressure_epsilon
  LOGICAL :: verbose ! verbose logging or not

  ! parameters for advection subroutine
  ! Courant numbers for both u and w and positive and negative
  REAL :: c_u_pos(0:cells_z + 1, 0:cells_x + 1), c_u_neg(0:cells_z + 1, 0:cells_x + 1)
  REAL :: c_w_pos(0:cells_z + 1, 0:cells_x + 1), c_w_neg(0:cells_z + 1, 0:cells_x + 1)
  ! Generic field F
  REAL :: F(0:cells_z + 1, 0:cells_x + 1), F_new(0:cells_z + 1, 0:cells_x + 1)

END MODULE parameters