function  one_cell_over_time(source_name, x_cell_index, z_cell_index)
    % Plots the property (source_name) of the desired cell over time.
    global cells_x ...
                del_x ...
                cells_z ...
                del_z ...
                total_iters ...
                del_t ...
                total_frames  ...
                output_freq_iter  ...
                output_freq_time  ...
                graph_aspect_ratio  ...
                use_every_nth_data_output ...
                add_asymmetry;
            
    source_matrix = importdata(source_name);
    source_matrix = source_matrix( z_cell_index:cells_z:end , x_cell_index);
    
    num_data_points = size(source_matrix,1);
    time_points = output_freq_iter*del_t* [0:num_data_points-1];
    plot( time_points ,source_matrix)
    
    ylabel("Property (FILL HERE)");
    xlabel("Time (seconds)");
    title(source_name + " changing over time for cell (" + x_cell_index + ", "+ z_cell_index +")");
    
    xticks(0:1000000:time_points(end));
    
    grid on
    grid minor
    
    
    %plot line to see when the asymmetry was introduced.
    xline(time_points(round(add_asymmetry/output_freq_iter)),'--r',{'Asymmetrical forcing began'});
    

end

