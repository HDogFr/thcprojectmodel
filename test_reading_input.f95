PROGRAM test_reading_input

INTEGER, PARAMETER :: cells_z = 17
INTEGER, PARAMETER :: cells_x = 33
REAL :: u(0:cells_z + 1, 0:cells_x + 1) ! x speed

INTEGER :: reason

OPEN(20, FILE = "u.dat", form = "formatted")

DO
    DO i = 0, cells_z + 1
        READ(20, '(101F12.6)', IOSTAT=reason) u(i, 0:cells_x + 1)
    END DO

    IF (.not. reason ==  0) THEN
        EXIT
    END if
END DO

WRITE(*,*) ( u(cells_z+1, k), k=0, cells_x+1)

CLOSE(20)

END PROGRAM test_reading_input