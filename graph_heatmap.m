function out = graph_heatmap(src, cells_z,total_frames,output_freq,del_t )
    source_matrix = importdata(src);
    
    h = heatmap(source_matrix(1 :cells_z ,:));
    h.GridVisible = 'off';
    % https://uk.mathworks.com/help/matlab/ref/colormap.html
    % jet gives a good range of colours, intead of just interpolating two
    h.Colormap = jet;
    
    % get rid of x labels
    cdl = h.XDisplayLabels;                                    % Current Display Labels
    h.XDisplayLabels = repmat(' ',size(cdl,1), size(cdl,2));   % Blank Display Labels
    % get rid of y labels
    cdl = h.YDisplayLabels;                                    % Current Display Labels
    h.YDisplayLabels = repmat(' ',size(cdl,1), size(cdl,2));   % Blank Display Labels
    
    for frame_counter = 0:total_frames-1
       % current_time_into_sim = frame_counter * output_freq * del_t
    
       %h.Title = "Percentage complete: " + frame_counter* 100 /(total_frames) + "%";
       h.Title = frame_counter * output_freq * del_t;
       h.ColorData = source_matrix(1 + cells_z*frame_counter:cells_z*(frame_counter+1) ,:);
       drawnow
    end
    
    out = h;
end